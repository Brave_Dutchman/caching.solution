﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Caching_Solution.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!, pressa key to continue");
            Console.ReadLine();

            var url = "https://localhost:10006/Get?url=https://localhost:10005/api/values";
            var container = new Container();

            var content = await container.Call(url);

            Console.WriteLine();
            Console.WriteLine(content ?? "No content");

            Console.ReadLine();
        }
    }

    public class Container
    {
        private readonly HttpClient _client;

        public Container()
        {
            _client = new HttpClient
            {
                Timeout = TimeSpan.FromSeconds(5)
            };
        }

        public async Task<string> Call(string url)
        {
            try
            {
                using (var response = await _client.GetAsync(url))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        return await _callAlternative(url);
                    }

                    return await response.Content.ReadAsStringAsync();
                };
            }
            catch (HttpRequestException)
            {
                return await _callAlternative(url);
            }
        }

        private async Task<string> _callAlternative(string url)
        {
            if (url.Contains("url="))
            {
                var alternativeUrl = url.Split("url=").Last();

                using (var response = await _client.GetAsync(alternativeUrl))
                {
                    if (!response.IsSuccessStatusCode)
                    {
                        return default;
                    }

                    return await response.Content.ReadAsStringAsync();
                };
            }

            return default;
        }
    }
}
