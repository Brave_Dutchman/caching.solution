﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Caching.Solution.Server.Controllers
{
    public class CacheController : Controller
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IHttpClientFactory _clientFactory;

        public CacheController(IDistributedCache distributedCache, IHttpClientFactory clientFactory)
        {
            _distributedCache = distributedCache;
            _clientFactory = clientFactory;
        }

        [HttpGet("get")]
        public async Task<IActionResult> Get(string url)
        {
            if (!Uri.TryCreate(url, UriKind.Absolute, out _))
            {
                return BadRequest();
            }

            var data = await _distributedCache.GetAsync(url);

            if (data == null)
            {
                return await _cacheAndReturnAsync(url);
            }

            using (var stream = new MemoryStream(data))
            using (var reader = new BinaryReader(stream))
            {
                var contentType = reader.ReadString();
                var content = reader.ReadString();

                return new ContentResult()
                {
                    Content = content,
                    ContentType = contentType
                };
            }
        }

        private async Task<IActionResult> _cacheAndReturnAsync(string url)
        {
            using (var response = await _clientFactory.CreateClient().GetAsync(url))
            {
                if (!response.IsSuccessStatusCode)
                {
                    return new StatusCodeResult(StatusCodes.Status503ServiceUnavailable);
                }

                var contentType = response.Content.Headers.ContentType.MediaType;
                var content = await response.Content.ReadAsStringAsync();

                using (var stream = new MemoryStream())
                using (var writer = new BinaryWriter(stream))
                {
                    writer.Write(contentType);
                    writer.Write(content);

                    await _distributedCache.SetAsync(url, stream.ToArray(), new DistributedCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1)
                    });
                }

                return new ContentResult()
                {
                    Content = content,
                    ContentType = contentType
                };
            }
        }
    }
}
